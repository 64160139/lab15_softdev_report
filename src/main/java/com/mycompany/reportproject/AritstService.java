/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.reportproject;

import java.util.List;

/**
 *
 * @author UsergetTopTenAristByTotalPrice
 */
public class AritstService {
    public List<ArtistReport> getTopTenAristByTotalPrice(){
        AritstDao aritstDao =new AritstDao();
        return aritstDao.getTenAristByTotalPrice(10);
    }
    
    public List<ArtistReport> getTopTenAristByTotalPrice(String begin,String end){
        AritstDao aritstDao =new AritstDao();
        return aritstDao.getTenAristByTotalPrice(begin,end,10);
    }
}
